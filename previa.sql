-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 11-Fev-2016 às 22:12
-- Versão do servidor: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vcsprojetos`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetosmigrations`
--

CREATE TABLE `vcs_projetosmigrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_banners`
--

CREATE TABLE `vcs_projetos_banners` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_banners`
--

INSERT INTO `vcs_projetos_banners` (`id`, `ordem`, `imagem1`, `imagem2`, `created_at`, `updated_at`) VALUES
(1, 0, '_DSC3013_20160211205619.jpg', '_JBF1771_20160211205620.jpg', '2016-02-11 22:56:21', '2016-02-11 22:56:21'),
(2, 0, '_JBF1923_20160211205628.jpg', '516_IMG_01_20160211205630.jpg', '2016-02-11 22:56:30', '2016-02-11 22:56:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_clipping`
--

CREATE TABLE `vcs_projetos_clipping` (
`id` int(10) unsigned NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_clipping`
--

INSERT INTO `vcs_projetos_clipping` (`id`, `imagem`, `titulo`, `data`, `slug`, `created_at`, `updated_at`) VALUES
(1, '_JBF1799_20160211205732.jpg', 'Exemplo', '2016-02', 'exemplo', '2016-02-11 22:57:33', '2016-02-11 22:57:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_clipping_imagem_fixa`
--

CREATE TABLE `vcs_projetos_clipping_imagem_fixa` (
`id` int(10) unsigned NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_clipping_imagem_fixa`
--

INSERT INTO `vcs_projetos_clipping_imagem_fixa` (`id`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '516_IMG_01_20160211205710.jpg', '0000-00-00 00:00:00', '2016-02-11 22:57:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_clipping_imagens`
--

CREATE TABLE `vcs_projetos_clipping_imagens` (
`id` int(10) unsigned NOT NULL,
  `clipping_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `altura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_clipping_imagens`
--

INSERT INTO `vcs_projetos_clipping_imagens` (`id`, `clipping_id`, `ordem`, `imagem`, `largura`, `altura`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '_JBF1771_20160211205745.jpg', '1980', '1293', '2016-02-11 22:57:46', '2016-02-11 22:57:47'),
(2, 1, 0, '_DSC3013_20160211205745.jpg', '1980', '1320', '2016-02-11 22:57:46', '2016-02-11 22:57:47'),
(3, 1, 0, '_JBF1768_20160211205745.jpg', '1980', '1323', '2016-02-11 22:57:46', '2016-02-11 22:57:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_contato`
--

CREATE TABLE `vcs_projetos_contato` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vitoria_telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vitoria_endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vitoria_googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `saopaulo_telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saopaulo_endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saopaulo_googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_contato`
--

INSERT INTO `vcs_projetos_contato` (`id`, `email`, `vitoria_telefone`, `vitoria_endereco`, `vitoria_googlemaps`, `saopaulo_telefone`, `saopaulo_endereco`, `saopaulo_googlemaps`, `twitter`, `facebook`, `linkedin`, `instagram`, `created_at`, `updated_at`) VALUES
(1, 'contato@vcsprojetos.com.br', '27 3324·0391', '<p>Rua Aleixo Neto, 1149 · 2° andar</p><p>Praia do Canto</p><p>29055·145 · Vitória · ES</p>', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3742.0689649258366!2d-40.29638268427029!3d-20.29741235508284!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xb817eeb1d521fb%3A0x6916967e29b1ba38!2sR.+Aleixo+Netto%2C+1149+-+Praia+do+Canto%2C+Vit%C3%B3ria+-+ES!5e0!3m2!1spt-BR!2sbr!4v1454446966966" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '11 4949·9552', '<p>Av. Brigadeiro Faria Lima, 1.461 · cj. 41</p><p>Jardim Paulistano</p><p>01452·002 · São Paulo · SP</p><p>Caixa postal 258</p>', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d117016.46392695229!2d-46.6760836531832!3d-23.576900170951614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce570b214d703b%3A0x8a145ae90fce2845!2sAv.+Brg.+Faria+Lima%2C+1461+-+Jardim+Paulistano%2C+S%C3%A3o+Paulo+-+SP%2C+01452-002!5e0!3m2!1spt-BR!2sbr!4v1454446987801" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'https://twitter.com/viviancoser', 'https://www.facebook.com/VCS-Projetos-Arquitetura-Urbanismo-e-Design-259419950783314/', 'https://br.linkedin.com/in/vivian-coser-b180896', 'https://www.instagram.com/viviancoser/', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_contatos_recebidos`
--

CREATE TABLE `vcs_projetos_contatos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_contatos_recebidos`
--

INSERT INTO `vcs_projetos_contatos_recebidos` (`id`, `nome`, `email`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(1, 'Teste', 'teste@teste.com', 'mensagem', 0, '2016-02-11 23:05:19', '2016-02-11 23:05:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_migrations`
--

CREATE TABLE `vcs_projetos_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_migrations`
--

INSERT INTO `vcs_projetos_migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_02_02_204032_create_banners_table', 1),
('2016_02_02_205324_create_perfil_table', 1),
('2016_02_02_205332_create_contato_table', 1),
('2016_02_02_205340_create_contatos_recebidos_table', 1),
('2016_02_03_145215_create_clipping_table', 1),
('2016_02_03_183058_create_projetos_table', 1),
('2016_02_11_153727_create_clipping_imagem_fixa_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_password_resets`
--

CREATE TABLE `vcs_projetos_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_perfil`
--

CREATE TABLE `vcs_projetos_perfil` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_perfil`
--

INSERT INTO `vcs_projetos_perfil` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam rhoncus gravida erat, quis fermentum augue congue ut. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec posuere mi in urna placerat, vel auctor urna ullamcorper. Vivamus ut ullamcorper tellus. Nulla maximus magna non purus bibendum maximus eget non purus. Suspendisse facilisis ante sed tempor dictum. Pellentesque porta lectus at tortor blandit condimentum. Donec eleifend bibendum justo, in fermentum elit scelerisque ac.</p>\r\n\r\n<p>Suspendisse potenti. Suspendisse posuere ante quis interdum vulputate. Sed luctus facilisis suscipit. Maecenas sed tincidunt est. Sed eget ex ut enim consectetur elementum. Aliquam erat volutpat. Integer hendrerit dolor ante, non mollis turpis imperdiet ac. Vivamus placerat mauris ut leo consectetur, ac imperdiet justo finibus. Proin placerat porttitor dolor quis congue. Aenean tincidunt, est a facilisis pellentesque, quam purus convallis metus, et viverra metus dui ac nibh. Mauris convallis porttitor augue, fermentum ultrices elit accumsan sed. Integer magna felis, iaculis cursus tincidunt nec, viverra vitae quam. Ut metus nunc, imperdiet at tortor at, placerat tempor mi. Aliquam erat volutpat.</p>\r\n', '_JBF1799_20160211205702.jpg', '0000-00-00 00:00:00', '2016-02-11 22:57:03');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_projetos`
--

CREATE TABLE `vcs_projetos_projetos` (
`id` int(10) unsigned NOT NULL,
  `projetos_categoria_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_projetos`
--

INSERT INTO `vcs_projetos_projetos` (`id`, `projetos_categoria_id`, `ordem`, `imagem`, `titulo`, `local`, `ano`, `slug`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '_DSC3013_20160211205929.jpg', 'Exemplo', 'SP', '2016', 'exemplo', '2016-02-11 22:59:30', '2016-02-11 22:59:30'),
(2, 1, 0, '_JBF1768_20160211205935.jpg', 'Exemplo', 'SP', '2016', 'exemplo-1', '2016-02-11 22:59:36', '2016-02-11 22:59:36'),
(3, 1, 0, '_JBF1799_20160211205940.jpg', 'Exemplo', 'SP', '2016', 'exemplo-2', '2016-02-11 22:59:42', '2016-02-11 22:59:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_projetos_categorias`
--

CREATE TABLE `vcs_projetos_projetos_categorias` (
`id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_projetos_categorias`
--

INSERT INTO `vcs_projetos_projetos_categorias` (`id`, `ordem`, `titulo`, `slug`, `created_at`, `updated_at`) VALUES
(1, 0, 'comercial', 'comercial', '2016-02-11 22:58:06', '2016-02-11 22:58:06'),
(2, 1, 'residencial', 'residencial', '2016-02-11 22:58:12', '2016-02-11 22:58:12'),
(3, 2, 'imobiliário', 'imobiliario', '2016-02-11 22:58:17', '2016-02-11 22:58:17'),
(4, 3, 'design de produto', 'design-de-produto', '2016-02-11 22:58:26', '2016-02-11 22:58:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_projetos_imagens`
--

CREATE TABLE `vcs_projetos_projetos_imagens` (
`id` int(10) unsigned NOT NULL,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `largura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `altura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_projetos_imagens`
--

INSERT INTO `vcs_projetos_projetos_imagens` (`id`, `projeto_id`, `ordem`, `imagem`, `largura`, `altura`, `created_at`, `updated_at`) VALUES
(1, 3, 0, '_DSC3013_20160211205949.jpg', '1980', '1320', '2016-02-11 22:59:51', '2016-02-11 22:59:51'),
(2, 3, 0, '_JBF1768_20160211205949.jpg', '1980', '1323', '2016-02-11 22:59:51', '2016-02-11 22:59:51'),
(3, 2, 0, '_JBF1771_20160211205953.jpg', '1980', '1293', '2016-02-11 22:59:55', '2016-02-11 22:59:55'),
(4, 2, 0, '_JBF1799_20160211205953.jpg', '1980', '1322', '2016-02-11 22:59:55', '2016-02-11 22:59:55'),
(5, 1, 0, '516_IMG_01_20160211210000.jpg', '1800', '1350', '2016-02-11 23:00:01', '2016-02-11 23:00:01'),
(6, 1, 0, '_JBF1923_20160211210000.jpg', '1980', '1321', '2016-02-11 23:00:01', '2016-02-11 23:00:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vcs_projetos_users`
--

CREATE TABLE `vcs_projetos_users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `vcs_projetos_users`
--

INSERT INTO `vcs_projetos_users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$D7ykDdI74bP8d2TagsXjWusROlAMXn4hupR98VoGQ0hClEj56YRRu', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vcs_projetos_banners`
--
ALTER TABLE `vcs_projetos_banners`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcs_projetos_clipping`
--
ALTER TABLE `vcs_projetos_clipping`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcs_projetos_clipping_imagem_fixa`
--
ALTER TABLE `vcs_projetos_clipping_imagem_fixa`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcs_projetos_clipping_imagens`
--
ALTER TABLE `vcs_projetos_clipping_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `clipping_imagens_clipping_id_foreign` (`clipping_id`);

--
-- Indexes for table `vcs_projetos_contato`
--
ALTER TABLE `vcs_projetos_contato`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcs_projetos_contatos_recebidos`
--
ALTER TABLE `vcs_projetos_contatos_recebidos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcs_projetos_password_resets`
--
ALTER TABLE `vcs_projetos_password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `vcs_projetos_perfil`
--
ALTER TABLE `vcs_projetos_perfil`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcs_projetos_projetos`
--
ALTER TABLE `vcs_projetos_projetos`
 ADD PRIMARY KEY (`id`), ADD KEY `projetos_projetos_categoria_id_foreign` (`projetos_categoria_id`);

--
-- Indexes for table `vcs_projetos_projetos_categorias`
--
ALTER TABLE `vcs_projetos_projetos_categorias`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vcs_projetos_projetos_imagens`
--
ALTER TABLE `vcs_projetos_projetos_imagens`
 ADD PRIMARY KEY (`id`), ADD KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`);

--
-- Indexes for table `vcs_projetos_users`
--
ALTER TABLE `vcs_projetos_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vcs_projetos_banners`
--
ALTER TABLE `vcs_projetos_banners`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vcs_projetos_clipping`
--
ALTER TABLE `vcs_projetos_clipping`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vcs_projetos_clipping_imagem_fixa`
--
ALTER TABLE `vcs_projetos_clipping_imagem_fixa`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vcs_projetos_clipping_imagens`
--
ALTER TABLE `vcs_projetos_clipping_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vcs_projetos_contato`
--
ALTER TABLE `vcs_projetos_contato`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vcs_projetos_contatos_recebidos`
--
ALTER TABLE `vcs_projetos_contatos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vcs_projetos_perfil`
--
ALTER TABLE `vcs_projetos_perfil`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vcs_projetos_projetos`
--
ALTER TABLE `vcs_projetos_projetos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vcs_projetos_projetos_categorias`
--
ALTER TABLE `vcs_projetos_projetos_categorias`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vcs_projetos_projetos_imagens`
--
ALTER TABLE `vcs_projetos_projetos_imagens`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vcs_projetos_users`
--
ALTER TABLE `vcs_projetos_users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `vcs_projetos_clipping_imagens`
--
ALTER TABLE `vcs_projetos_clipping_imagens`
ADD CONSTRAINT `clipping_imagens_clipping_id_foreign` FOREIGN KEY (`clipping_id`) REFERENCES `vcs_projetos_clipping` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `vcs_projetos_projetos`
--
ALTER TABLE `vcs_projetos_projetos`
ADD CONSTRAINT `projetos_projetos_categoria_id_foreign` FOREIGN KEY (`projetos_categoria_id`) REFERENCES `vcs_projetos_projetos_categorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `vcs_projetos_projetos_imagens`
--
ALTER TABLE `vcs_projetos_projetos_imagens`
ADD CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `vcs_projetos_projetos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
