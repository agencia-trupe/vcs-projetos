<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClippingImagemFixa extends Model
{
    protected $table = 'clipping_imagem_fixa';

    protected $guarded = ['id'];
}
