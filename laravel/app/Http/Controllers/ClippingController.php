<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Clipping;
use App\Models\ClippingImagemFixa;

class ClippingController extends Controller
{
    public function index()
    {
        $clipping   = Clipping::ordenados()->get();
        $imagemFixa = ClippingImagemFixa::first();

        return view('frontend.clipping', compact('clipping', 'imagemFixa'));
    }
}
