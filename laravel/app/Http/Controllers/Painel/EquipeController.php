<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EquipeRequest;
use App\Http\Controllers\Controller;

use App\Models\Equipe;
use App\Helpers\CropImage;

class EquipeController extends Controller
{
    private $image_config = [
        'width'  => 150,
        'height' => 150,
        'path'   => 'assets/img/equipe/'
    ];

    public function index()
    {
        $registros = Equipe::ordenados()->get();

        return view('painel.equipe.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.equipe.create');
    }

    public function store(EquipeRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Equipe::create($input);
            return redirect()->route('painel.equipe.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Equipe $registro)
    {
        return view('painel.equipe.edit', compact('registro'));
    }

    public function update(EquipeRequest $request, Equipe $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $registro->update($input);
            return redirect()->route('painel.equipe.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Equipe $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.equipe.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
