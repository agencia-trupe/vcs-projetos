<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BannerRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Helpers\CropImage;

class BannersController extends Controller
{
    private $image_config = [
        'imagem1' => [
            'width'  => 1980,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/banners/1/'
        ],
        'imagem2' => [
            'width'  => 720,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/banners/2/'
        ]
    ];

    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('painel.banners.index', compact('banners'));
    }

    public function create()
    {
        return view('painel.banners.create');
    }

    public function store(BannerRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem1'] = CropImage::make('imagem1', $this->image_config['imagem1']);
            $input['imagem2'] = CropImage::make('imagem2', $this->image_config['imagem2']);

            Banner::create($input);
            return redirect()->route('painel.banners.index')->with('success', 'Banner adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar banner: '.$e->getMessage()]);

        }
    }

    public function edit(Banner $banner)
    {
        return view('painel.banners.edit', compact('banner'));
    }

    public function update(BannerRequest $request, Banner $banner)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem1'])) $input['imagem1'] = CropImage::make('imagem1', $this->image_config['imagem1']);
            if (isset($input['imagem2'])) $input['imagem2'] = CropImage::make('imagem2', $this->image_config['imagem2']);

            $banner->update($input);
            return redirect()->route('painel.banners.index')->with('success', 'Banner alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar banner: '.$e->getMessage()]);

        }
    }

    public function destroy(Banner $banner)
    {
        try {

            $banner->delete();
            return redirect()->route('painel.banners.index')->with('success', 'Banner excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir banner: '.$e->getMessage()]);

        }
    }
}
