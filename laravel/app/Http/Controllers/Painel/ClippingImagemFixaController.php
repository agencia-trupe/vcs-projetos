<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClippingImagemFixaRequest;
use App\Http\Controllers\Controller;

use App\Models\ClippingImagemFixa;
use App\Helpers\CropImage;

class ClippingImagemFixaController extends Controller
{
    private $image_config = [
        'width'  => null,
        'height' => 1280,
        'path'   => 'assets/img/clipping/'
    ];

    public function index()
    {
        $imagem = ClippingImagemFixa::first();

        return view('painel.clipping.imagem', compact('imagem'));
    }

    public function update(ClippingImagemFixaRequest $request, ClippingImagemFixa $imagem)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $imagem->update($input);
            return redirect()->route('painel.clipping.imagem.index')->with('success', 'Imagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar imagem: '.$e->getMessage()]);

        }
    }
}
