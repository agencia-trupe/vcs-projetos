<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Perfil;
use App\Models\Equipe;

class PerfilController extends Controller
{
    public function index()
    {
        $perfil = Perfil::first();

        return view('frontend.perfil', compact('perfil'));
    }

    public function equipe()
    {
        $equipe = Equipe::ordenados()->get();

        return view('frontend.equipe', compact('equipe'));
    }
}
