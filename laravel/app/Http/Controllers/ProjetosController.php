<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index(ProjetoCategoria $categoriaSelecionada)
    {
        if (! $categoriaSelecionada->exists) {
            $categoriaSelecionada = ProjetoCategoria::ordenados()->first() ?: \App::abort('404');
        }

        $projetos = $categoriaSelecionada->projetos()->get();

        view()->share('categorias', ProjetoCategoria::ordenados()->get());
        view()->share('categoriaSelecionada', $categoriaSelecionada);

        return view('frontend.projetos', compact('projetos'));
    }
}
