<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('perfil/equipe', ['as' => 'perfil.equipe', 'uses' => 'PerfilController@equipe']);
Route::get('perfil', ['as' => 'perfil', 'uses' => 'PerfilController@index']);
Route::get('projetos/{projetos_categoria_slug?}', ['as' => 'projetos', 'uses' => 'ProjetosController@index']);
Route::get('imprensa', ['as' => 'imprensa', 'uses' => 'ClippingController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);
    Route::resource('equipe', 'EquipeController');
    Route::resource('banners', 'BannersController');
    Route::resource('perfil', 'PerfilController');
    Route::resource('projetos/categorias', 'ProjetosCategoriasController');
    Route::resource('projetos', 'ProjetosController');
    Route::get('projetos/{projetos}/imagens/clear', [
        'as'   => 'painel.projetos.imagens.clear',
        'uses' => 'ProjetosImagensController@clear'
    ]);
    Route::resource('projetos.imagens', 'ProjetosImagensController');
    Route::resource('clipping/imagem', 'ClippingImagemFixaController');
    Route::resource('clipping', 'ClippingController');
    Route::get('clipping/{clipping}/imagens/clear', [
        'as'   => 'painel.clipping.imagens.clear',
        'uses' => 'ClippingImagensController@clear'
    ]);
    Route::resource('clipping.imagens', 'ClippingImagensController');
    Route::resource('contato/recebidos', 'ContatosRecebidosController');
    Route::resource('contato', 'ContatoController');
    Route::resource('usuarios', 'UsuariosController');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
