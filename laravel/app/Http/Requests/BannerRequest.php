<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'imagem1' => 'required|image',
            'imagem2' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem1'] = 'image';
            $rules['imagem2'] = 'image';
        }

        return $rules;
    }
}
