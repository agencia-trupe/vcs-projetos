<?php

namespace App\Helpers;

class Tools
{

    public static function formataData($data = '')
    {
        $meses = array(
            '01' => 'JANEIRO',
            '02' => 'FEVEREIRO',
            '03' => 'MARÇO',
            '04' => 'ABRIL',
            '05' => 'MAIO',
            '06' => 'JUNHO',
            '07' => 'JULHO',
            '08' => 'AGOSTO',
            '09' => 'SETEMBRO',
            '10' => 'OUTUBRO',
            '11' => 'NOVEMBRO',
            '12' => 'DEZEMBRO'
        );

        if($data != '') {
            list($mes, $ano) = explode('/', $data);
            return $meses[$mes].' · '.$ano;
        } else {
            return $data;
        }
    }

}
