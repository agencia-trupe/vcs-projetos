@extends('frontend.common.template')

@section('content')

    <div class="not-found">
        <div class="wrapper">
            <div class="content">
                <h1>Página não encontrada</h1>
            </div>
        </div>
    </div>

@endsection
