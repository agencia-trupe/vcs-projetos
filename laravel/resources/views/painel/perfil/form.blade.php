@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($perfil->imagem)
    <img src="{{ url('assets/img/perfil/'.$perfil->imagem) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
