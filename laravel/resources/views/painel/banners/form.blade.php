@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem1', 'Imagem Principal') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/1/'.$banner->imagem1) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 600px;">
        @endif
            {!! Form::file('imagem1', ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem2', 'Imagem Secundária') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/banners/2/'.$banner->imagem2) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 200px;">
        @endif
            {!! Form::file('imagem2', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
