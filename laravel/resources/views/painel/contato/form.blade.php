@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('vitoria_telefone', 'Telefone Vitória') !!}
            {!! Form::text('vitoria_telefone', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('vitoria_endereco', 'Endereço Vitória') !!}
            {!! Form::textarea('vitoria_endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('vitoria_googlemaps', 'Código GoogleMaps Vitória') !!}
            {!! Form::text('vitoria_googlemaps', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('saopaulo_telefone', 'Telefone São Paulo') !!}
            {!! Form::text('saopaulo_telefone', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('saopaulo_endereco', 'Endereço São Paulo') !!}
            {!! Form::textarea('saopaulo_endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('saopaulo_googlemaps', 'Código GoogleMaps São Paulo') !!}
            {!! Form::text('saopaulo_googlemaps', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('twitter', 'Twitter (opcional)') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook (opcional)') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('linkedin', 'Linkedin (opcional)') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram (opcional)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
