@extends('frontend.common.template')

@section('content')

    <div class="clipping">
        <div class="imagem-fixa" style="background-image:url({{ asset('assets/img/clipping/'.$imagemFixa->imagem) }})"></div>

        <div class="clipping-thumbs grid">
            @foreach($clipping as $registro)
            <a href="#" class="thumb" data-galeria="{{ $registro->id }}">
                <img src="{{ asset('assets/img/clipping/thumbs/'.$registro->imagem) }}" alt="">
                <div class="overlay">
                    <div class="overlay-content-wrapper">
                        <p>
                            {{ $registro->titulo }}
                            <span>{{ Tools::formataData($registro->data) }}</span>
                        </p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <script>
        var galerias = [];
        @foreach($clipping as $registro)
            galerias[{{ $registro->id }}] = [];
            @foreach($registro->imagens as $imagem)
                galerias[{{ $registro->id }}].push({
                    src   : '{{ asset('assets/img/clipping/imagens/'.$imagem->imagem) }}',
                    title : '{{ $registro->titulo }} &middot; {{ Tools::formataData($registro->data) }}',
                    w     : {{ $imagem->largura ?: 0 }},
                    h     : {{ $imagem->altura ?: 0 }},
                });
            @endforeach
        @endforeach
    </script>

@endsection
