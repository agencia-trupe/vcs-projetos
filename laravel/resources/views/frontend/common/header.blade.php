    <header>
        <h1>{{ config('site.name') }}</h1>

        <nav id="nav-desktop">
            @include('frontend.common.nav')
        </nav>
        @if(Route::currentRouteName() == 'projetos' && isset($categorias))
            <div class="categorias">
            @foreach($categorias as $categoria)
                <a href="{{ route('projetos', $categoria->slug) }}" @if($categoriaSelecionada->id == $categoria->id) class="active" @endif>{{ $categoria->titulo }}</a>
            @endforeach
            </div>
        @endif
        @if(str_is('perfil*', Route::currentRouteName()))
            <div class="categorias">
                <a href="{{ route('perfil') }}" @if(Route::currentRouteName() == 'perfil') class="active" @endif>sobre</a>
                <a href="{{ route('perfil.equipe') }}" @if(Route::currentRouteName() == 'perfil.equipe') class="active" @endif>equipe</a>
            </div>
        @endif

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>

    </header>

    <nav id="nav-mobile">
        @include('frontend.common.nav')
    </nav>
