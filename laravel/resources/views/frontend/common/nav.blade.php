<a href="{{ route('home') }}" @if(Route::currentRouteName() == 'home') class="active" @endif>home</a>
<a href="{{ route('perfil') }}" @if(str_is('perfil*', Route::currentRouteName())) class="active" @endif>perfil</a>
<a href="{{ route('projetos') }}" @if(Route::currentRouteName() == 'projetos') class="active" @endif>projetos</a>
<a href="{{ route('imprensa') }}" @if(Route::currentRouteName() == 'imprensa') class="active" @endif>imprensa</a>
<a href="http://cliente.viviancoser.com.br/" target="_blank">área do cliente</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>contato</a>
