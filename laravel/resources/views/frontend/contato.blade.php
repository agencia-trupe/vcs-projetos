@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="vitoria">
            <div class="info">
                <div class="info-wrapper">
                    <div class="info-content">
                        <div class="texto">
                            <h2>VITÓRIA</h2>
                            <p class="telefone">{{ $contato->vitoria_telefone }}</p>
                            <div class="endereco">
                                {!! $contato->vitoria_endereco !!}
                            </div>
                        </div>

                        <form action="" id="form-contato" method="POST">
                            <h2>FALE CONOSCO</h2>
                            <input type="text" name="nome" id="nome" placeholder="nome" required>
                            <input type="email" name="email" id="email" placeholder="e-mail" required>
                            <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                            <input type="submit" value="enviar">
                            <div id="form-contato-response"></div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="googlemaps">
                {!! $contato->vitoria_googlemaps !!}
            </div>
        </div>

        <div class="saopaulo">
            <div class="info">
                <div class="info-wrapper">
                    <div class="info-content">
                        <div class="texto">
                            <h2>SÃO PAULO</h2>
                            <p class="telefone">{{ $contato->saopaulo_telefone }}</p>
                            <div class="endereco">
                                {!! $contato->saopaulo_endereco !!}
                            </div>
                        </div>

                        <div class="redes-sociais">
                            @if($contato->twitter)<a href="{{ $contato->twitter }}" target="_blank" class="social social-twitter">twitter</a>@endif
                            @if($contato->facebook)<a href="{{ $contato->facebook }}" target="_blank" class="social social-facebook">facebook</a>@endif
                            @if($contato->linkedin)<a href="{{ $contato->linkedin }}" target="_blank" class="social social-linkedin">linkedin</a>@endif
                            @if($contato->instagram)<a href="{{ $contato->instagram }}" target="_blank" class="social social-instagram">instagram</a>@endif

                            <p class="copyright">
                                © {{ date('Y') }} {{ config('site.name') }} · Todos os direitos reservados.<br>
                                <a href="http://trupe.net" target="_blank">Criação de Sites</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="googlemaps">
                {!! $contato->saopaulo_googlemaps !!}
            </div>
        </div>
    </div>

@endsection
