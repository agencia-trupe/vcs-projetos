@extends('frontend.common.template')

@section('content')

    <div class="equipe">
        <div class="categorias-mobile">
            <a href="{{ route('perfil') }}" @if(Route::currentRouteName() == 'perfil') class="active" @endif>sobre</a>
            <a href="{{ route('perfil.equipe') }}" @if(Route::currentRouteName() == 'perfil.equipe') class="active" @endif>equipe</a>
        </div>

        <div class="bg-fixo"></div>

        <div class="equipe-thumbs">
            @foreach($equipe as $pessoa)
            <div class="thumb">
                <img src="{{ asset('assets/img/equipe/'.$pessoa->imagem) }}" alt="">
                <div class="texto">
                    <h3>
                        {{ $pessoa->nome }}
                        @if($pessoa->cargo)
                        - <span>{{ $pessoa->cargo }}</span>
                        @endif
                    </h3>
                    <p>{{ $pessoa->texto }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
