@extends('frontend.common.template')

@section('content')

    <script>
        var galerias = [];
        @foreach($projetos as $registro)
            galerias[{{ $registro->id }}] = [];
            @foreach($registro->imagens as $imagem)
                galerias[{{ $registro->id }}].push({
                    src   : '{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}',
                    title : '{{ $registro->titulo }} &middot; {{ $registro->local }} &middot; {{ $registro->ano }}',
                    w     : {{ $imagem->largura ?: 0 }},
                    h     : {{ $imagem->altura ?: 0 }},
                });
            @endforeach
        @endforeach
    </script>

    <div class="projetos">
        <div class="categorias-mobile">
        @foreach($categorias as $categoria)
            <a href="{{ route('projetos', $categoria->slug) }}" @if($categoriaSelecionada->id == $categoria->id) class="active" @endif>{{ $categoria->titulo }}</a>
        @endforeach
        </div>

        @if(count($projetos))
        <div class="projetos-principais grid">
            @foreach($projetos->splice(0, 2) as $registro)
            <a href="#" class="thumb" data-galeria="{{ $registro->id }}" style="background-image:url('{{ asset('assets/img/projetos/thumbs/big/'.$registro->imagem) }}')">
                <div class="overlay">
                    <div class="overlay-content-wrapper">
                        <p>
                            {{ $registro->titulo }}
                            <span>{{ $registro->local }} &middot; {{ $registro->ano }}</span>
                        </p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>

        <div class="projetos-thumbs grid">
            @foreach($projetos as $registro)
            <a href="#" class="thumb" data-galeria="{{ $registro->id }}">
                <img src="{{ asset('assets/img/projetos/thumbs/'.$registro->imagem) }}" alt="">
                <div class="overlay">
                    <div class="overlay-content-wrapper">
                        <p>
                            {{ $registro->titulo }}
                            <span>{{ $registro->local }} &middot; {{ $registro->ano }}</span>
                        </p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
        @endif
    </div>

@endsection
