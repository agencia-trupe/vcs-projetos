@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner">
            <div class="banner-imagem banner-imagem-1" style="background-image:url({{ asset('assets/img/banners/1/'.$banner->imagem1) }})"></div>
            <div class="banner-imagem banner-imagem-2" style="background-image:url({{ asset('assets/img/banners/2/'.$banner->imagem2) }})"></div>
        </div>
        @endforeach
    </div>

@endsection
