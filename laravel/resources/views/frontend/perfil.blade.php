@extends('frontend.common.template')

@section('content')

    <div class="perfil">
        <div class="categorias-mobile">
            <a href="{{ route('perfil') }}" @if(Route::currentRouteName() == 'perfil') class="active" @endif>sobre</a>
            <a href="{{ route('perfil.equipe') }}" @if(Route::currentRouteName() == 'perfil.equipe') class="active" @endif>equipe</a>
        </div>

        <div class="texto">
            {!! $perfil->texto !!}
        </div>

        <div class="foto" style="background-image:url({{ asset('assets/img/perfil/'.$perfil->imagem) }})">
            <img src="{{ asset('assets/img/perfil/'.$perfil->imagem) }}" alt="">
        </div>
    </div>

@endsection
