(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
            speed: 1500
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut('fast', function() {
                    $response.text(data.message).fadeIn('slow');
                });
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut('fast', function() {
                    $response.text('Preencha todos os campos corretamente').fadeIn('slow');
                });
            },
            dataType: 'json'
        });
    };

    App.thumbPhotoswipe = function() {
        var $grid = $('.grid');
        if (!$grid.length) return;

        $('.thumb').click(function(e) {
            e.preventDefault();
            var id = $(this).data('galeria');
            if(id){
                var pswpElement = document.querySelectorAll('.pswp')[0];
                var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, galerias[id], {
                    shareEl: false,
                    fullscreenEl: false,
                    counterEl: false
                });
                gallery.init();
            }
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        $('#form-contato').on('submit', this.envioContato);
        this.thumbPhotoswipe();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
