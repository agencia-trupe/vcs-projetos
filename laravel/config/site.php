<?php

return [

    'name'        => 'VCS Projetos',
    'title'       => 'Vivian Coser – Arquitetos Associados',
    'description' => 'Vivian Coser Arquitetos Associados: escritório especializado em projetos residenciais, comerciais, corporativos e empreendimentos imobiliários; reformas de apartamentos, casas, escritórios.',
    'keywords'    => 'arquitetura, arquitetura de interiores, decoração, reforma, projetos residenciais, projetos corporativos',
    'share_image' => 'share.png',
    'analytics'   => 'UA-75721909-1'

];
