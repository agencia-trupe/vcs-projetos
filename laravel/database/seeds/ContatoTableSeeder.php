<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'               => 'contato@vcsprojetos.com.br',
            'vitoria_telefone'    => '27 3324·0391',
            'vitoria_endereco'    => '<p>Rua Aleixo Neto, 1149 · 2° andar</p><p>Praia do Canto</p><p>29055·145 · Vitória · ES</p>',
            'vitoria_googlemaps'  => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3742.0689649258366!2d-40.29638268427029!3d-20.29741235508284!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xb817eeb1d521fb%3A0x6916967e29b1ba38!2sR.+Aleixo+Netto%2C+1149+-+Praia+do+Canto%2C+Vit%C3%B3ria+-+ES!5e0!3m2!1spt-BR!2sbr!4v1454446966966" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'saopaulo_telefone'   => '11 4949·9552',
            'saopaulo_endereco'   => '<p>Av. Brigadeiro Faria Lima, 1.461 · cj. 41</p><p>Jardim Paulistano</p><p>01452·002 · São Paulo · SP</p><p>Caixa postal 258</p>',
            'saopaulo_googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d117016.46392695229!2d-46.6760836531832!3d-23.576900170951614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce570b214d703b%3A0x8a145ae90fce2845!2sAv.+Brg.+Faria+Lima%2C+1461+-+Jardim+Paulistano%2C+S%C3%A3o+Paulo+-+SP%2C+01452-002!5e0!3m2!1spt-BR!2sbr!4v1454446987801" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'twitter'             => 'https://twitter.com/viviancoser',
            'facebook'            => 'https://www.facebook.com/VCS-Projetos-Arquitetura-Urbanismo-e-Design-259419950783314/',
            'linkedin'            => 'https://br.linkedin.com/in/vivian-coser-b180896',
            'instagram'           => 'https://www.instagram.com/viviancoser/',
        ]);
    }
}
