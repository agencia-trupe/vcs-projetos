<?php

use Illuminate\Database\Seeder;

class ClippingImagemFixaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clipping_imagem_fixa')->insert([
            'imagem' => 'imagem'
        ]);
    }
}
